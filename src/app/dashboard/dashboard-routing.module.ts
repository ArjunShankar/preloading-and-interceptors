import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children: [
      {path: '', pathMatch: 'full', redirectTo: 'products'},
      {
        path: 'products',
        loadChildren: () => import('../products/products.module').then(m => m.ProductsModule),
        data: { preload: true }},
      {
        path: 'cart',
        loadChildren: () => import('../cart/cart.module').then(m => m.CartModule),
        data: { preload: false }}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
})

export class DashboardRoutingModule { }
