import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules, NoPreloading} from '@angular/router';
import {OptInPreloadStrategy} from './core/strategies/opt-in-preload-strategy';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    data: { preload: true }
  },
  { path: 'aboutus',
    loadChildren: () => import('./aboutus/aboutus.module').then(m => m.AboutusModule),
    data: { preload: false }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
    //  {preloadingStrategy: PreloadAllModules}
    //  {preloadingStrategy: NoPreloading}
    { preloadingStrategy: OptInPreloadStrategy }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
